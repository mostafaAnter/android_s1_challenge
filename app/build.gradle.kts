plugins {
    id ("com.android.application")
    id ("kotlin-android")

    //for data binding and dagger and more
    id("kotlin-kapt")

    //for hilt
    id("dagger.hilt.android.plugin")

    //for google services
    id("com.google.gms.google-services")

    //for crashlytics
    id("com.google.firebase.crashlytics")
}

// to apply detekt code analysis on app module
apply { from(rootProject.file("detekt.gradle")) }

android {
    signingConfigs {
        create("config") {
            storeFile = file("../Untitled")
            storePassword = "password"
            keyAlias = "key0"
            keyPassword = "password"
        }
    }
    compileSdkVersion (AppConfig.compileSdk)
    buildToolsVersion (AppConfig.buildToolsVersion)

    defaultConfig {
        applicationId("app.anter.android_s1_challenge")
        minSdkVersion (AppConfig.minSdk)
        targetSdkVersion (AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        // region multi dex support
        //multiDexEnabled = true
        // endregion
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        signingConfig = signingConfigs.getByName("config")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

    }
    //support data binding
    buildFeatures{
        dataBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    // region enable multi dex
    //implementation ("com.android.support:multidex:1.0.3")
    //endregion

    //for hilt app must depend on all modules
    //depend on features modules
    implementation(project(":core"))
    implementation(project(":feature_two_panels_host"))
    implementation(project(":feature_map_panel"))
    implementation(project(":feature_list_panel"))

    //region lib
    implementation (fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation (AppDependencies.kotlin_stdlib)
    implementation (AppDependencies.core_ktx)
    implementation (AppDependencies.androidx_appcompat)
    implementation (AppDependencies.google_material)
    implementation (AppDependencies.constraint_layout)
    implementation (AppDependencies.navigation_fragment)
    implementation (AppDependencies.navigation_ui)
    //endregion

    // region dagger
    implementation (AppDependencies.dagger_hilt)
    kapt (AppDependencies.dagger_kapt)
    // end region

    //region firebase
    implementation(platform(AppDependencies.firebase_bom))
    implementation (AppDependencies.firebase_analytics)
    implementation (AppDependencies.firebase_crashlytics)
    implementation (AppDependencies.firebase_messaging)
    //endregion

    //region event bus
    implementation (AppDependencies.event_bus)
    //endregion

    //region database
    implementation(AppDependencies.room_database)
    implementation(AppDependencies.room_database_ktx)
    kapt(AppDependencies.room_database_kapt)
    //endregion

    //region leak canary
    debugImplementation(AppDependencies.leak_canary)
    //endregion

    //region unit test and ui test
    // JUNIT
    testImplementation (AppDependencies.junit)
    androidTestImplementation (AppDependencies.junit_test)

    // ui test, instrumentation test
    // Espresso
    androidTestImplementation (AppDependencies.espresso_core)
    // androidx.test
    androidTestImplementation (AppDependencies.test_runner)
    androidTestImplementation (AppDependencies.test_rules)
    androidTestImplementation (AppDependencies.test_core)
    androidTestImplementation (AppDependencies.test_ext_junit)
    //endregion
}