plugins {
    id ("com.android.library")
    id ("kotlin-android")

    // fot dagger until now
    id("kotlin-kapt")
    //for hilt
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion (AppConfig.compileSdk)
    buildToolsVersion (AppConfig.buildToolsVersion)

    defaultConfig {
        minSdkVersion (AppConfig.minSdk)
        targetSdkVersion (AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    // enable data binding inside module
    dataBinding.apply {
        @Suppress("DEPRECATION")
        isEnabled = true
    }
}

dependencies {
    //depend on core module for string res
    implementation(project(":core"))

    implementation (AppDependencies.kotlin_stdlib)
    implementation (AppDependencies.core_ktx)
    implementation (AppDependencies.androidx_appcompat)
    implementation (AppDependencies.google_material)

    // constraint layout
    implementation (AppDependencies.legacy_support)
    implementation (AppDependencies.constraint_layout)

    //navigation
    implementation (AppDependencies.navigation_fragment)
    implementation (AppDependencies.navigation_ui)

    // region dagger
    implementation (AppDependencies.dagger_hilt)
    kapt (AppDependencies.dagger_kapt)
    // end region

    // region Coroutine
    implementation (AppDependencies.coroutine_android)
    implementation (AppDependencies.coroutine_core)
    //endregion

    //lifecycle scope
    implementation(AppDependencies.scopes_lifecycle_coroutines)
    implementation(AppDependencies.scopes_viewModel_coroutines)

    // region dagger
    implementation (AppDependencies.dagger_hilt)
    kapt (AppDependencies.dagger_kapt)
    // end region

    //region viewmodel lifecycle
    implementation(AppDependencies.hilt_viewModel_lifecycle)
    kapt(AppDependencies.hilt_viewModel_lifecycle_kapt)
    //endregion

    //region permission dispatcher
    implementation (AppDependencies.permission_dispatcher)
    kapt (AppDependencies.permission_dispatcher_kapt)
    //endregion

    //region event bus
    implementation (AppDependencies.event_bus)
    //endregion

    testImplementation (AppDependencies.junit)
    androidTestImplementation (AppDependencies.junit_test)
    androidTestImplementation (AppDependencies.espresso_core)
}