plugins {
    id ("com.android.library")
    id ("kotlin-android")

    // fot dagger until now
    kotlin("kapt")

    //for hilt
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion (AppConfig.compileSdk)
    buildToolsVersion (AppConfig.buildToolsVersion)

    defaultConfig {
        minSdkVersion (AppConfig.minSdk)
        targetSdkVersion (AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = true
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildTypes.forEach {
        it.buildConfigField("String", "API_BASE_URL", "\"https://base.url.com/\"")
        it.buildConfigField("String", "STREAM_API_KYE", "\"qx5us2v6xvmh\"")
    }
    // enable data binding inside module
    dataBinding.apply {
        @Suppress("DEPRECATION")
        isEnabled = true
    }
}

dependencies {

    implementation (AppDependencies.kotlin_stdlib)
    implementation (AppDependencies.core_ktx)
    implementation (AppDependencies.androidx_appcompat)
    implementation (AppDependencies.google_material)

    // region dagger
    implementation (AppDependencies.dagger_hilt)
    kapt (AppDependencies.dagger_kapt)
    // end region

    // region retrofit network calls
    implementation (AppDependencies.retrofit)
    implementation (AppDependencies.retrofit_converter)
    // for support android 4
    implementation (AppDependencies.okHttp_interceptor)
    implementation (AppDependencies.okHttp){
        version{
            strictly(Versions.okHttpVersion)
        }
    }
    // endregion

    // region Moshi very important to generate adapters converters
    implementation (AppDependencies.moshi)
    kapt(AppDependencies.moshi_kapt)
    // endregion

    // region Coroutine
    implementation (AppDependencies.coroutine_android)
    implementation (AppDependencies.coroutine_core)
    //endregion

    // coil image loader
    implementation(AppDependencies.coil)

    //region unit test and ui test
    testImplementation (AppDependencies.junit)
    androidTestImplementation (AppDependencies.junit_test)
    androidTestImplementation (AppDependencies.espresso_core)
    //endregion
}